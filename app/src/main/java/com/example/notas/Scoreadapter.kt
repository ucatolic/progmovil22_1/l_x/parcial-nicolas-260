package com.example.notas

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class Scoreadapter: RecyclerView.Adapter<Scoreadapter.ViewHolder>() {
    val materia = arrayOf("INGLÉS INDEPENDIENTE ","INGLÉS INDEPENDIENTE AVANZADO ","FUNDAMENTACIÓN MATEMÁTICA ",
        "EXPRESIÓN ORAL Y ESCRITA ",
        "PRESEMINARIO ","ALGORITMIA Y PROGRAMACIÓN ","PENSAMIENTO SISTÉMICO E INNOVACIÓN ","INTRODUCCIÓN A LA INGENIERÍA ",
        "CÁLCULO DIFERENCIAL ","ÁLGEBRA LINEAL ","PROGRAMACIÓN IMPERATIVA ","ESTRUCTURAS DISCRETAS ","FOTOGRAFÍA DIGITAL ",
        "CÁLCULO INTEGRAL ",
        "MECÁNICA Y LABORATORIO ","ANTROPOLOGÍA FILOSÓFICA ","ESTRUCTURAS DE DATOS ",
        "DISEÑO Y PROGRAMACIÓN ORIENTADA A OBJETOS ","MECÁNICA Y LABORATORIO ",
        "PROBABILIDAD Y ESTADÍSTICA ","ÓPTICA ONDAS Y LABORATORIO ","ÉTICA GENERAL ",
        "BASES DE DATOS ","ECUACIONES DIFERENCIALES ",
        "CULTURA CATÓLICA ","SISTEMAS OPERATIVOS ","AUTÓMATAS Y LENGUAJES ","GUITARRA LATINOAMERICANA ",
        "ELECTRICIDAD Y MAGNETISMO Y LABORATORIO ",
        "FILOSOFÍA DEL ARTE ","INFORMÁTICA SOCIAL ","INGENIERÍA DE SOFTWARE ","REDES DE COMUNICACIONES ",
        "INGENIERÍA ECONÓMICA ",
        "REDES Y SERVICIOS ","SISTEMAS DE INFORMACIÓN ","CONSTRUCCIÓN DE SOFTWARE ","ANÁLISIS Y DISEÑO DE ALGORITMOS ",
        "FORMULACIÓN Y EVALUACIÓN DE PROYECTOS ","ARQUITECTURA DE SOFTWARE ","ALGORITMOS Y ANÁLISIS ESTOCÁSTICO ",
        "INGENIERÍA WEB")
    val score = arrayOf("10,0","8,5","9,6","9,0","9,3","9,6","8,7","8,0","6,9","7,3","9,2","6,8","6,9","9,3","5,4","7,2","7,9","7,6",
    "7,3","6,3","6,5","7,6","8,7","6,3","9,8","9,5","9,6","8,7","10,0","7,2","9,0","8,9","6,0","7,0","7,9","7,6","6,9","9,5","9,1","6,0",
    "8,0","7,5","8,5")
    //val prof = arrayOf("")
    val images = arrayOf(R.drawable.ingles, R.drawable.ingles, R.drawable.calculo, R.drawable.oral, R.drawable.uca,
        R.drawable.proy,
        R.drawable.proy, R.drawable.calculo, R.drawable.calculo,R.drawable.calculo, R.drawable.algorit, R.drawable.algorit,
        R.drawable.foto, R.drawable.calculo, R.drawable.fis, R.drawable.uca, R.drawable.algorit,
        R.drawable.algorit, R.drawable.fis , R.drawable.calculo, R.drawable.fis, R.drawable.uca,
        R.drawable.proy, R.drawable.calculo, R.drawable.uca, R.drawable.proy, R.drawable.automatas, R.drawable.guit,
        R.drawable.fis, R.drawable.uca, R.drawable.proy, R.drawable.proy, R.drawable.automatas,
        R.drawable.calculo, R.drawable.automatas, R.drawable.proy, R.drawable.algorit, R.drawable.algorit,
        R.drawable.proy,R.drawable.proy,R.drawable.proy,R.drawable.proy)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.materias, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvv1.text=materia[position]
        holder.tvv2.text=score[position]
        holder.image.setImageResource(images[position])
    }

    override fun getItemCount(): Int {
        return materia.size
    }
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        var tvv1: TextView
        var tvv2: TextView
        var image: ImageView
        init {
            tvv1 = itemView.findViewById(R.id.tvv1)
            tvv2 = itemView.findViewById(R.id.tvv2)
            image= itemView.findViewById(R.id.itemImage)
        }
    }
}