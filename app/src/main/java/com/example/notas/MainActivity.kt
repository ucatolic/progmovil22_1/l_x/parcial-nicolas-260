package com.example.notas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.master)
        setSupportActionBar(findViewById(R.id.toolbar))
        val btnAvanzar:Button = findViewById(R.id.btnNext)
        btnAvanzar.setOnClickListener {
            checkName()
        }
        /*val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = Scoreadapter()
*/
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.Home -> {
            val intent = Intent(this, Dev::class.java)
            intent.putExtra("INTENT_NOMBRE", "Nicolas Javier Cardozo Diaz")
            intent.putExtra("INTENT_CODIGO", "67000260")
            startActivity(intent)
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
    fun checkName () {
        val etName: EditText = findViewById(R.id.idName)
        val etCode: EditText = findViewById(R.id.idCode)
        val etMail: EditText = findViewById(R.id.idMail)

        if (etName.text.isNotEmpty() && etCode.text.isNotEmpty() && etMail.text.isNotEmpty()) {
            // Cambiar entre pantallas
            val intent = Intent(this, Master::class.java)
            //intent.putExtra("INTENT_NAME", etName.text)
            startActivity(intent)
        } else {
            // Mensaje de error toast
            Toast.makeText(this, "valide campos", Toast.LENGTH_SHORT).show()
        }
    }

}